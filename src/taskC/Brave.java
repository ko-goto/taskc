package taskC;

import java.util.ArrayList;
import java.util.Arrays;

public class Brave {

	private final String name;

	private final Integer[] freeDays;

	private ArrayList<String> metBraves;

	public Brave(String name, Integer[] freeDays) {
		this.name = name;
		this.freeDays = freeDays;
		this.metBraves = new ArrayList<String>(Arrays.asList(name));
	}

	public ArrayList<String> getMetBraves(){
		return this.metBraves;
	}

	public String getName() {
		return this.name;
	}

	public boolean isFreeDays(int day) {
		return Arrays.asList(this.freeDays).contains(day);
	}

	public void addMetBravesAll(ArrayList<String> metBraves) {
		this.metBraves.addAll(metBraves);
	}

}

