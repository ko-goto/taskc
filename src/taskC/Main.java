package taskC;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;

import org.apache.commons.lang3.StringUtils;

public class Main {

	public static void main(String[] args) {

        System.out.println("入力してください。");

        try(Scanner sc1 = new Scanner(System.in) ){

        	ArrayList<Integer> results = new ArrayList<Integer>();

	        // セット数
	        int line1 = Integer.parseInt(sc1.nextLine());


	        for(int i= 0; i < line1; i++) {

	        	ArrayList<Brave> braves = new ArrayList<Brave>();

	        	HashSet<Integer> freeDaysAll = new HashSet<>();

	        	// 勇者の数
	        	int line2 = Integer.parseInt(sc1.nextLine());

	        	for(int j= 0; j < line2; j++) {

		        	// 空いている日付(半角スペース区切)
		        	String line3 = sc1.nextLine();

		        	String[] freeDays = line3.split(" ", 0);

		        	Integer[] days = new Integer[freeDays.length];

		        	for (int k = 0; k < freeDays.length; k++) {

		        		String freeDay = freeDays[k];

		        		if(StringUtils.isNotEmpty(freeDay) && StringUtils.isNumeric(freeDay)) {
		        			int day = Integer.parseInt(freeDay);

		        			days[k] = day;

		        			freeDaysAll.add(day);
		        		}

		        	}

		        	braves.add(new Brave(String.valueOf(j), days));

	        	}

	        	int result = -1;

	        	outside : for(int day : freeDaysAll) {

	        		for(Brave brave1 : braves) {

	        			// 勇者の空いている日付と一致する場合
	        			if(brave1.isFreeDays(day)) {

    	        			// 勇者を検索する
	    	        		for(Brave brave2 : braves) {

	    	        			String name = brave2.getName();
	    	        			// 自身以外の勇者を検索する
	    	        			if(!brave1.getName().equals(name)) {

	    	        				// 日付が一致する かつ 今まであったことのない勇者の場合
	    	        				if((brave2.isFreeDays(day)) && (!brave1.getMetBraves().contains(name))) {

	    	        					// 前の値を退避する
	    	        					ArrayList<String> oldMetBraves = (ArrayList<String>) brave1.getMetBraves().clone();

	    	        					// 自身に相手の会ったことのある勇者を全て追加する
	    	        					brave1.addMetBravesAll(brave2.getMetBraves());
	    	        					// 相手に自身の会ったことのある勇者を全て追加する
	    	        					brave2.addMetBravesAll(oldMetBraves);

	    	        					// 勇者の数と一致した日を地図が集まった日とする
	    	        					if(brave1.getMetBraves().size() >= line2) {
	    	        						result = day;
	    	        						break outside;
	    	        					}
	    	        				}
	    	        			}
	    	        		}
	        			}
	        		}
	        	}
	        	results.add(result);
	        }

	        System.out.println("結果");

	        for(int result : results) {
	        	System.out.println(result);
	        }
        }

	}
}
